﻿using System;
using System.Runtime.InteropServices;
using WebSocketSharp;

namespace client
{
  class Client
  {
    /** @SignalHandler */
    private static SignalHandler signalHandler;
    
    /** @WebSocket */
    private static WebSocket _ws;
    
    /**
     * Стартовая точка приложения
     */
    public static void Main(string[] args)
    {
      // Получаем событие о закрытии программы и обрабатываем его
      signalHandler += HandleConsoleSignal;
      ConsoleHelper.SetSignalHandler(signalHandler, true);
      
      // Получаем логин
      Console.WriteLine("Enter Your name: ");
      var name = Console.ReadLine();

      // Eсли name не пустая строка устанавливаем как имя
      var url = !String.IsNullOrEmpty(name) 
        ? "ws://localhost:8008/Chat?name=" + name 
        : "ws://localhost:8008/Chat";

      // Cоздаем WebSocket
      using (_ws = new WebSocket(url))
      {
        // Cобытие (ответ от сервера)
        _ws.OnMessage += (sender, e) => Console.WriteLine(e.Data);

        // Подключаемся к серверу
        _ws.Connect();

        // Eсли подключились запускаем ChatLoop()
        if (_ws.ReadyState.ToString() == "Open")
        {
          Console.WriteLine("Connec to server" );
          ChatLoop();
          _ws.Close();
        }
        Console.WriteLine("Can not connect to server");
        _ws.Close();
      }
    }

    /**
     * Обработчик закрытия программы
     */
    private static void HandleConsoleSignal(ConsoleSignal consoleSignal)
    {
      Console.WriteLine(consoleSignal);
      Console.WriteLine("Disconnected");
      _ws.Close();
    }
    
    /**
     * Цыкл чата
     */
    private static void ChatLoop()
    {
      while (true)
      {
        // Отправляем то что ввел в консоль пользователь
        _ws.Send(Console.ReadLine());
      }
    }
  }
  
  internal delegate void SignalHandler(ConsoleSignal consoleSignal);

  /**
   * Виды окончания работы приложения
   */
  internal enum ConsoleSignal
  {
    CtrlC = 0,
    CtrlBreak = 1,
    Close = 2,
    LogOff = 5,
    Shutdown = 6
  }

  /**
   * Обрабатываем событие
   */
  internal static class ConsoleHelper
  {
    [DllImport("Kernel32", EntryPoint = "SetConsoleCtrlHandler")]
    public static extern bool SetSignalHandler(SignalHandler handler, bool add);
  }
  
}