﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace server
{
    class Server
    {
        /** @SignalHandler */
        private static SignalHandler signalHandler;
        
        /**
         * Стартовая точка приложения
         */
        static void Main(string[] args)
        {
            // Получаем событие о закрытии программы и обрабатываем его
            signalHandler += HandleConsoleSignal;
            ConsoleHelper.SetSignalHandler(signalHandler, true);
            
            // Создаем WebSocket
            var wssv = new WebSocketServer("ws://localhost:8008");

            // Подключаем модули
            wssv.AddWebSocketService<ServerLogic>("/"); // Простой пинг-понг чат
            wssv.AddWebSocketService<Chat>("/Chat"); // Простой многопользовательский чат

            // Запускаем сервер
            wssv.Start();
            
            // Перечисляем наши модули в консоль
            if (wssv.IsListening)
            {
                Console.WriteLine("Listening on port {0}, on address and {1}, providing WebSocket services:", wssv.Port,
                    wssv.Address);
                foreach (var path in wssv.WebSocketServices.Paths)
                    Console.WriteLine("-{2}:{1}{0}", path, wssv.Port, wssv.Address);
            }

            // Ждем нажатие любой клавиши что бы отключить сервер
            // @todo требуется написать мануал с доступными командами для сервере (help, exit, etc..)
            Console.ReadKey(true);
            wssv.Stop();
            Console.WriteLine("Stop Server");
        }
        
        /**
         * Обработчик закрытия программы
         */
        private static void HandleConsoleSignal(ConsoleSignal consoleSignal)
        {
            Console.WriteLine(consoleSignal);
        }
    }

    /**
     * Простой echo chat
     */
    public class ServerLogic : WebSocketBehavior
    {
        // Событие входящее сообщение
        protected override void OnMessage(MessageEventArgs e)
        {
            // пишем в консоль
            Console.WriteLine(e.Data);
            // отправляем echo сообщение
            Send(e.Data);
        }
    }

    /**
     * Простой многопользовательский чат
     */
    public class Chat : WebSocketBehavior
    {
        private string _name; // Имя пользователя
        private static int _number = 0; // Номер
        private string _prefix; // префикс

        // что это и зачем ?
        public Chat()
            : this(null)
        {
        }

        /**
         * Установка префикс (имя или anon#)
         */
        public Chat(string prefix)
        {
            _prefix = !prefix.IsNullOrEmpty() ? prefix : "anon#";
        }

        /**
         * Получить имя и установить его в префикс
         */
        private string GetName()
        {
            // Получаем имя из конекста при подключении клиента ("ws://localhost/Chat?name=TestName" )
            var name = Context.QueryString["name"];
            return !name.IsNullOrEmpty() ? name : _prefix + GetNumber();
        }

        /**
         * Получить инкриментированое число
         */
        private static int GetNumber()
        {
            // Обычно автоинкримент выглядит как i++
            return Interlocked.Increment(ref _number);
        }

        /**
         * Тригер на отключение клиента
         */
        protected override void OnClose(CloseEventArgs e)
        {
            // Сообщаем всем что _name отключился
            Sessions.Broadcast(String.Format("{0} got logged off...", _name));
        }

        /**
         * Тригер на сообщение
         */
        protected override void OnMessage(MessageEventArgs e)
        {
            // Отправляем всем сообщение от пользователя _name
            Sessions.Broadcast(String.Format("{0}: {1}", _name, e.Data));
        }

        /**
         * Тригер на подключение
         */
        protected override void OnOpen()
        {
            // Устанавливаем имя подключившегося
            _name = GetName();
        }

        /**
         * Обработка ошибок
         */
        protected override void OnError(ErrorEventArgs e)
        {
            Console.WriteLine(e.Exception);
        }
    }
    

    internal delegate void SignalHandler(ConsoleSignal consoleSignal);
    
    /**
     * Виды окончания работы приложения
     */
    internal enum ConsoleSignal
    {
        CtrlC = 0,
        CtrlBreak = 1,
        Close = 2,
        LogOff = 5,
        Shutdown = 6
    }

    /**
     * Обрабатываем событие
     */
    internal static class ConsoleHelper
    {
        [DllImport("Kernel32", EntryPoint = "SetConsoleCtrlHandler")]
        public static extern bool SetSignalHandler(SignalHandler handler, bool add);
    }
}